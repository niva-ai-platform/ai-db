# Database micro-service

This folder contains the database micro-service.


## Implementation

This implementation uses MySql 8. It exposes port 3306 for client connections.

### Supported tables

This service supports the following tables:

- `grantschemes` table, listing the different types of grants available and a code to use when applying for a grant.

- `applications` table, listing the grant applications that have been started (and that photos can be uploaded to).

More to follow ...

### Building the container

----
cd ai-db
docker  -t ai-db/latest build .
----
