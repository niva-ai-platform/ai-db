FROM mysql:8.0

# Environment
ENV MYSQL_ROOT_PASSWORD=3aieou
ENV MYSQL_DATABASE=niva_ai_db

# ports
EXPOSE 3306

# Volumes
VOLUME /var/lib/mysql

# Default entrypoint
COPY ./conf/ /docker-entrypoint-initdb.d/