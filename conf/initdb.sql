
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Table structure for table `grantschemes`
--
DROP TABLE IF EXISTS `grantschemes`;
CREATE TABLE `grantschemes` (
  `id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_general_ci NOT NULL UNIQUE,
  /*`counter` int(11) NOT NULL, */
  `name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `valid_from` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `valid_until` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `grantschemes`
--

INSERT INTO `grantschemes` (`id`, `code`, `name`) VALUES
(1, 'aes', 'Agri-Environment Schemes (AES)'),
(2, 'heps', 'Horticulture Exceptional Payment Scheme (HEPS)'),
(3, 'papcm', 'Protein Aid and Protein/Cereal Mix (50/50) Crop Scheme'),
(4, 'tis', 'Tillage Incentive Scheme');


--
-- Table structure for table `applications`
--

DROP TABLE IF EXISTS `applications`;
CREATE TABLE `applications` (
  `id` int(11) NOT NULL,
  `appid` varchar(255) NOT NULL,
  `author` varchar(255),
  `schemeid` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


--
-- Test data for table `applications`
--

INSERT INTO `applications` (`id`, `appid`, `author`, `schemeid`) VALUES
(5001, '202206-305001', 'me', 'aes'),
(5002, '202207-015002', 'me', 'aes'),
(5003, '202207-015003', 'me', 'tis');


--
-- Table structure for table `photoevidence`
--

DROP TABLE IF EXISTS `photoevidence`;
CREATE TABLE `photoevidence` (
  `id` int(11) NOT NULL,
  `appid` varchar(255) NOT NULL,
  `author` varchar(255),
  `filename` varchar(1024) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Test data for table `photoevidence`
--

INSERT INTO `photoevidence` (`id`, `appid`, `filename`) VALUES
(1001, '202206-305001', '/uploads/202206-305001/1001.jpg'),
(1002, '202207-015002', '/uploads/202207-015002/1002.jpg'),
(1003, '202207-015002', '/uploads/202207-015002/1003.jpg');


--
-- Table structure for table `ai_results`
--

DROP TABLE IF EXISTS `ai_results`;
CREATE TABLE `ai_results` (
  `id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `passed` boolean NOT NULL,
  `confidence` float NOT NULL,
  `ai_module_name` varchar(255) NOT NULL,
  `ai_model` varchar(255) NOT NULL,
  `results` json,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
  /*,
  FOREIGN KEY (`filename`) REFERENCES photoevidence(`filename`)*/
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


--
-- Table structure for table `ai_results`
--

DROP TABLE IF EXISTS `ai_module_registry`;
CREATE TABLE `ai_module_registry` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `stage` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `update_image` boolean,
  `authenticated` boolean NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


--
-- Table structure for table `tags`
--

-- DROP TABLE IF EXISTS `tags`;
-- CREATE TABLE `tags` (
--   `id` int(11) NOT NULL,
--   `name` varchar(255) NOT NULL,
--   `lang` varchar(255) NOT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


--
-- Table structure for table `image_tags`
--

DROP TABLE IF EXISTS `image_tags`;
CREATE TABLE `image_tags` (
  `id` int(11) NOT NULL,
  `img_id` varchar(255) NOT NULL,
  `tag` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


--
-- Indexes for dumped tables
--

--
-- Indexes for table `applications`
--

ALTER TABLE `applications`
  ADD PRIMARY KEY (`id`),
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5004;


--
-- Indexes for table `photoevidence`
--
ALTER TABLE `photoevidence`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_appid` (`appid`),
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1004;


--
-- Indexes for table `ai_results`
--

ALTER TABLE `ai_results`
  ADD PRIMARY KEY (`id`),
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1001;


--
-- Indexes for table `ai_module_registry`
--

ALTER TABLE `ai_module_registry`
  ADD PRIMARY KEY (`id`),
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1001;


-- ALTER TABLE `tags`
--   ADD PRIMARY KEY (`id`),
--   MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1001;

ALTER TABLE `image_tags`
  ADD PRIMARY KEY (`id`),
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1001;

COMMIT;
